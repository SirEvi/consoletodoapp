﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TodoApp2._1.Models;

namespace TodoApp2._1.Database
{
    public class TodoDbContext : DbContext
    {
        public TodoDbContext()
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb; Database=TodoAppDb2.1; Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Person> DbPeople { get; set; }
        public DbSet<Task> DbTasks { get; set; }
        public DbSet<Todo> DbTodos { get; set; }
    }
}
