﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TodoApp2._1.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DbPeople",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbPeople", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTasks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "DbTodos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true),
                    TaskId = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    PersonId = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbTodos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DbTodos_DbPeople_PersonId",
                        column: x => x.PersonId,
                        principalTable: "DbPeople",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DbTodos_DbTasks_TaskId",
                        column: x => x.TaskId,
                        principalTable: "DbTasks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DbTodos_PersonId",
                table: "DbTodos",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_DbTodos_TaskId",
                table: "DbTodos",
                column: "TaskId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DbTodos");

            migrationBuilder.DropTable(
                name: "DbPeople");

            migrationBuilder.DropTable(
                name: "DbTasks");
        }
    }
}
