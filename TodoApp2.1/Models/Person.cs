﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApp2._1.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
