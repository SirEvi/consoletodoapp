﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApp2._1.Models
{
    public enum Status
    {
        Inprogress,

        Started,

        OnHold,

        Updated,

        Deleted
    }
}
