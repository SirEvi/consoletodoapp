﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApp2._1.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
