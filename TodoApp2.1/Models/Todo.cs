﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoApp2._1.Models
{
    public class Todo
    {
        public Todo()
        {
            //Tasks = new List<Task>();
            //Persons = new List<Person>();
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public virtual Task Tasks{ get; set; }
        public int TaskId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public virtual Person Person { get; set; }
        public int PersonId { get; set; }
        public Status State { get; set; }

    }
}
