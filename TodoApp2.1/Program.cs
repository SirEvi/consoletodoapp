﻿using System;
using TodoApp2._1.Services;

namespace TodoApp2._1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("****************** TODO APPLICATION ********************");
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();
            Console.WriteLine(@" Welecome {0}", name);
            Console.WriteLine("What do you want to do today? ");
            Console.WriteLine("_______________________________");

            // Console Menu
            Console.WriteLine("****** MENU ********");
            Console.WriteLine(" Add, View, Change OR Delete Item");
            Console.WriteLine("**********************");

            //string command = "Add Eat 22/06/2020";

            Console.Write("For Help Enter 'Help': ");
            //string help = Console.ReadLine();

            string UserInput = Console.ReadLine();
            string helpDesk = "1) To Add Item ==> Add Task Date Individual.\n" +
                        "2) To Delete An Item ==> Delete Task Date Individual.\n" +
                        "3) To View Item ==> View Task Date Individual.\n" +
                        "4) To Change An Item ==> Change Task Date Individual.\n" +
                        "5) To Exit ==> Hit The Enter Key.";

            do
            {
                if (UserInput == "Help")
                {
                    // Display Menu
                    Console.WriteLine(helpDesk);
                }
                else
                {
                    Console.WriteLine(@"You Enter A Wrong Command, Enter 'Help' not {0} ", UserInput);
                    UserInput = Console.ReadLine();
                    Console.WriteLine(helpDesk);
                }

            }
            while (UserInput != "Help");
            Console.WriteLine("____________________________________________________");

            // Input String
            Console.Write("Enter Your Command: ");
            string data = Console.ReadLine();

            string[] myList = data.Split(' ');

            string MyTask = myList[1];
            string Date = myList[2];
            string MyUser = myList[3];

            // Coverting Date string
            DateTime MyDate = DateTime.Parse(Date);

            string[] com = new string[] { "Add", "View", "Delete", "Change" };
            var commandx = myList[0];


            switch (commandx)
            {
                case "Add":
                    Console.WriteLine(" You are about to create a Todo");
                    TodoOps.CreateTodo(MyTask, MyDate, MyUser);
                    break;

                case "View":
                    Console.WriteLine(" You are about to view a Todo");
                    TodoOps.ViewTodo(MyTask, MyDate, MyUser);
                    break;

                case "Delete":
                    Console.WriteLine(" You are about to delete a Todo");
                    TodoOps.DeleteTodo(MyTask, MyDate, MyUser);
                    break;

                case "Change":
                    Console.WriteLine(" You are about to change a Todo");
                    TodoOps.ChangeTodo(MyTask, MyDate, MyUser);
                    break;

            }

            //AddTask(str1);
            Console.WriteLine("******** Click Enter to Close ************* ");
            Console.ReadLine();
        }
    }
}
