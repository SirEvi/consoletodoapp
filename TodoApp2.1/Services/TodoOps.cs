﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TodoApp2._1.Database;
using TodoApp2._1.Models;

namespace TodoApp2._1.Services
{
    public class TodoOps
    {
        //    // Create Todo Task
        public static void CreateTodo(string name, DateTime time, string user)
        {
            try
            {
                using (var addTodo = new TodoDbContext())
                {

                    Task task = new Task() { Name = name };
                    Person taskUser = new Person { Name = user };
                    var todos = addTodo.DbTasks.ToList();
                    var findName = todos.FirstOrDefault(x => x.Name.Trim().ToLower() == name.Trim().ToLower());
                    if (findName == null)
                    {
                        Todo newTodo = new Todo { Description = name, Person = taskUser, Tasks = task, StartDate = time, State = Status.Started };
                        addTodo.DbTodos.Add(newTodo);
                        addTodo.SaveChanges();
                        Console.WriteLine(" Todo task created ");
                    }
                    else
                    {
                        Console.WriteLine(" Todo already exist");
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception($"Something funny happened that i cannot explain : Reason ==> { ex.Message}");
            }

        }

        // Change Todo Task
        public static void ChangeTodo(string name, DateTime time, string user)
        {
            try
            {
                using (var changeTodo = new TodoDbContext())
                {

                    var todos = changeTodo.DbTodos.ToList();
                    var todo = todos.Find(t => t.Description.Trim().ToLower() == name.Trim().ToLower());
                    if (todo != null)
                    {
                        Task taskUpdate = new Task { Id = todo.TaskId, Name = name };
                        Person userUpdate = new Person { Id = todo.PersonId, Name = user };
                        todo.Person = userUpdate;
                        todo.Tasks = taskUpdate;
                        todo.StartDate = time;
                       
                        changeTodo.DbTodos.Update(todo);
                        changeTodo.SaveChanges();
                        Console.WriteLine(" Todo task updated ");
                    }
                    else
                    {
                        Console.WriteLine("Todo task not found.");
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Something funny happened that i cannot explain : Reason ==> { ex.Message}");
            }

        }

        // View Todo Task
        public static void ViewTodo(string name, DateTime date, string user)
        {
            try
            {
                using (var viewTodo = new TodoDbContext())
                {
                    var todos = viewTodo.DbTodos.ToList();
                    var findId = todos.FirstOrDefault(x => x.Description.Trim().ToLower() == name.Trim().ToLower() && x.StartDate.ToShortDateString() == date.ToShortDateString());
                    if (findId == null)
                    {
                        Console.WriteLine("Record Not Found!");
                    }
                    else
                    {
                        Console.WriteLine($"Description: {findId.Description}, TaskId: {findId.TaskId}, StartDate: {findId.StartDate} & Status: {findId.State}");
                        Console.WriteLine(" Todo task displayed. ");
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception($"Something funny happened that i cannot explain: Reason ==> { ex.Message }");
            }

        }

        // Delete Todo Task
        public static void DeleteTodo(string name, DateTime date, string user)
        {
            try
            {
                using (var delTodo = new TodoDbContext())
                {

                    var todos = delTodo.DbTodos.ToList();
                    var todo = todos.FirstOrDefault(x => x.Description.Trim().ToLower() == name.Trim().ToLower() && x.StartDate.ToShortDateString() == date.ToShortDateString());
                    if (todo != null)
                    {
                        delTodo.DbTodos.Remove(todo);
                        delTodo.SaveChanges();
                        Console.WriteLine(" Todo task deleted ");
                    }
                    else
                    {
                        Console.WriteLine("Record Not Found!");
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception($"Something funny happened that i cannot explain: Reason ==> { ex.Message }");
            }

        }
    }
}
